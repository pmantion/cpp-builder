#!/usr/bin/bash

BUILD_DIR=${BUILD_DIR:-cache}
ACC_HOST=${ACC_HOST:-'cwe-513-vol956.cern.ch:'}
RSYNC_OPTS=
#RSYNC_OPTS='-z'

# Get dependencies from /acc
function fetch_dependency()
{
    function usage()
    {
        echo "Usage:"
        echo "    $0 <ACC_PATH> [--force]"
    }
    if [[ $# -lt 1 ]] || [[ $# -gt 2 ]]
    then
        usage
        return
    fi
    if [[ $# == 2 ]]
    then
        if [[ "$2" == "--force" ]]
        then
            local force=true
        else
            usage
            return
        fi
    fi
    echo "Force: $force"
    local dep_path="$1"
    local destination="${BUILD_DIR}${dep_path}"
    if [[ ${processed} =~ "${dep_path}"  ]]
    then
        echo "Skip: ${dep_path} already processed"
        return
    elif [[ -e "${destination}" ]] && [[ -z "$force" ]]
    then
        echo "Skip: '${destination}' exists, skipping retrieval of '${dep_path}'"
        return
    else
        echo "Copying '${dep_path}' to '${destination}'"
        mkdir -p $(dirname ${destination})
        rsync -a --info=progress2 ${RSYNC_OPTS} ${ACC_HOST}${dep_path} $(dirname ${destination})
    fi

    local processed="${processed}:${dep_path}"
    echo "Processed : $processed"
    # Resolve & retrieve symbolic links, for e.g. in accsoft-ds-fwk or cmw-fwk
    find "${destination}" -type l | while read symlink
    do
        # Skip copying boost again and again (mostly because of a symlink to boost in cmw-rda3)
        #if [[ "$(basename ${symlink})" = "boost" || "$(basename ${symlink}})" = "libboost*" ]]
        #then
        #    continue
        #fi
        local link_dest=$(readlink ${symlink})
        echo "   Symlink found: ${symlink} -> ${link_dest}"
        if [[ ${link_dest} == /* ]]
        then
            resolved_symlink=${link_dest}
        else
            local symlink_dir=$(dirname ${symlink})
            resolved_symlink="${symlink_dir#"$BUILD_DIR"}/${link_dest}"
        fi
        echo "   Resolving symlink '${symlink}' as '${resolved_symlink}'"
        fetch_dependency ${resolved_symlink}
    done
}
