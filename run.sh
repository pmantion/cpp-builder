#!/usr/bin/bash

set -eux

# Flags explanation
#
# -u root : It enables to write volume when running rootless podman as non-root user: https://www.tutorialworks.com/containers/podman-rootless-volumes/.
#            This flag should be removed when running with docker. 
podman run -ti  \
    -u root \
    -v $PWD/../workspace:/workspace:Z    \
    -v $PWD/cache/acc/local:/acc/local:ro                      \
    -v $PWD/cache/acc/sys/cdk:/acc/sys/cdk:ro                  \
    -v $PWD/cache/acc/sys/Linux:/acc/sys/Linux:ro              \
    --security-opt seccomp=unconfined                          \
    accsoft-cpp-light
