# A lightweight (i.e. with a *thin* `/acc`) container to build C++ projects

## Run

1. Create a local copy of `/acc/local`, `/acc/sys/cdk` and `/acc/sys/Linux`:
    
    ``./update_dep_cache.sh``

2. Run: **see `run.sh`

    ``
    ./run.sh
    ``
