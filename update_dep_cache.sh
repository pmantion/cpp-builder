#!/usr/bin/bash

set -eu

BUILD_DIR=${BUILD_DIR:-cache}
ACC_HOST=${ACC_HOST:-'cwe-513-vol956.cern.ch:'}
#RSYNC_OPTS='-z'
CONTAINER_CMD=${CONTAINER_CMD:-'podman'}

source fetch_dep_helper.sh

function fetch_deps() {
    # Base toolchain with makefile-generic
    fetch_dependency /acc/sys/cdk/L867
    fetch_dependency /acc/local/share/makefile-generic/3.x.x
    fetch_dependency /acc/local/share/makefile-generic/2.x.x
    fetch_dependency /acc/local/L867/3rdparty/google/gtest
    fetch_dependency /acc/local/L867/3rdparty/google/gmock
    # New gcc toolchain
    fetch_dependency /acc/local/Linux/x86_64-linux-gcc/current/gcc
    fetch_dependency /acc/local/Linux/x86_64-linux-gcc/current/Make.variables
    # Add codeine
    fetch_dependency /acc/local/L867/accsoft/accsoft-codeine
    fetch_dependency /acc/local/share/accsoft/accsoft-codeine
    fetch_dependency /acc/sys/Linux/toolchain_libs

    # Add boost as a very common dependency
    fetch_dependency /acc/local/L867/3rdparty/boost/1.69.0
    fetch_dependency /acc/local/share/fesa/fesa-makefile/3.3.1
    # extend the use of the image to build accsoft-ds-server:
    fetch_dependency /acc/local/L867/accsoft/accsoft-ds-model/2.0.0
    fetch_dependency /acc/local/L867/cmw/cmw-log/4.0.1
    fetch_dependency /acc/local/L867/cmw/cmw-util/5.0.1
    fetch_dependency /acc/local/L867/cmw/cmw-rda3/5.1.1
    # back to fesa deps:
    fetch_dependency /acc/local/L867/accsoft/accsoft-ds-fwk/DEV
    fetch_dependency /acc/local/L867/cmw/cmw-rda3/4.0.0
    # For branch 7.x of FESA
    fetch_dependency /acc/local/L867/cmw/cmw-data/3.0.0
    fetch_dependency /acc/local/L867/cmw/cmw-log/3.7.0
    fetch_dependency /acc/local/L867/cmw/cmw-util/4.0.0
    fetch_dependency /acc/local/L867/cmw/cmw-rda3/3.2.0
    # For dev on codeine
    fetch_dependency /acc/local/L867/accsoft/accsoft-commons/dev
}

fetch_deps
